
#include <iostream>
#include <string>
#include "doctorType.h"

using namespace std; 

void doctorType::print() const
{
    personType::print();
    cout << " " << speciality;
}

string doctorType::getSpeciality()
{
    return speciality;
}

void doctorType::setSpeciality(string spl) //Debug 04 -- Missing setSpeciality function
	{
		speciality = spl;
	}

doctorType::doctorType(string first, string last, string spl)
            :personType(first, last)
{
    speciality = spl;
}
 
